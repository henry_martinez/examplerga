package com.example.hmartinez.myapplication;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;

/**
 *
 * Created by hmartinez on 9/19/15.
 */
public class ColorMeterView extends View {

    private Bitmap mBitmap;
    private Bitmap mBitmapBlur;
    private Paint mPaint;
    private RectF mOval = new RectF();
    private Paint mTextPaint;
    private int mProgress = 0;
    private int mMax = 100;
    private float mArcAngle = 360 * 0.95f;
    private Typeface customFont;
    private Shader ringShader;
    private String mSuffixText = "Km";
    private Matrix m;
    private RectF src = new RectF();
    private RectF dst = new RectF();

    ValueAnimator mProgressAnimator;
    Interpolator interpolator = new AccelerateDecelerateInterpolator();
    private float mLastProgress;
    private long mDuration = 800;
    private float mValue;

    public ColorMeterView(Context context) {
        super(context);
        final Resources resources = getResources();
        mBitmap = BitmapFactory.decodeResource(resources, R.drawable.daily_goal_color_meter);
        mBitmapBlur = BitmapFactory.decodeResource(resources, R.drawable.daily_goal_ring_blur);
        customFont = Typeface.createFromAsset(getContext().getAssets(), "fonts/AkzidGrtskProBolCnd.otf");
        ringShader = new BitmapShader(mBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        init();
    }

    private void init() {
        m = new Matrix();
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setTypeface(customFont);
        mPaint.setShader(ringShader);
        initValueAnimator();
    }

    @Override
    protected void onMeasure(int w, int h) {
        setMeasuredDimension(w, h);
        int width = MeasureSpec.getSize(w);
        int height = MeasureSpec.getSize(h);
        m.reset();
        src.set(0, 0, mBitmap.getWidth(), mBitmap.getHeight());
        dst.set(0, 0, width, height);
        m.setRectToRect(src, dst, Matrix.ScaleToFit.CENTER);
        ringShader.setLocalMatrix(m);
        m.mapRect(mOval, src);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        float startAngle = 270 - mArcAngle / 2f;
        float sweepAngle = mValue / (float) getMax() * mArcAngle;

        canvas.drawArc(mOval, startAngle, sweepAngle, true, mPaint);
        canvas.drawBitmap(mBitmapBlur, null, mOval, null);

        String text = String.valueOf(Math.round(mValue));
        if (!TextUtils.isEmpty(text)) {
            mTextPaint.setTextSize(100);
            mTextPaint.setTextAlign(Paint.Align.CENTER);
            mTextPaint.setColor(0xffffffff);
            float textHeight = mTextPaint.descent() + mTextPaint.ascent();
            float textBaseline = (getHeight() - textHeight) / 2.0f;
            canvas.drawText(text +" "+ mSuffixText, getWidth() / 2.0f, textBaseline, mTextPaint);

        }
    }

    private void initValueAnimator(){
        mProgressAnimator = new ValueAnimator();
        mProgressAnimator.setInterpolator(interpolator);
        mProgressAnimator.addUpdateListener(new ProgressAnimatorListener());
    }

    private void animateProgress(){
        if (mProgressAnimator != null){
            mProgressAnimator.setFloatValues(mLastProgress, mProgress);
            mProgressAnimator.setDuration(mDuration);
            mProgressAnimator.start();
        }
    }

    @Override
    public void invalidate() {
        init();
        super.invalidate();
    }

    public int getProgress() {
        return mProgress;
    }

    public void setProgress(int progress) {
        this.mProgress = progress;
        if (this.mProgress > getMax()) {
            this.mProgress %= getMax();
        }
        animateProgress();
    }

    public int getMax() {
        return mMax;
    }

    public void setMax(int max) {
        if (max > 0) {
            this.mMax = max;
            invalidate();
        }
    }

    private class ProgressAnimatorListener implements ValueAnimator.AnimatorUpdateListener {

        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            Float value = (Float) animation.getAnimatedValue();
            setValue(value);
            mLastProgress = value;

            invalidate();

        }
    }

    private void setValue(float value) {
        this.mValue = value;
        if (this.mValue > getMax()) {
            this.mValue %= getMax();

        }
    }


}
